package Modul6;

import java.awt.Color;
import javax.swing.JFrame;

public class BintangTest {
    public static void main(String[] args){
        JFrame frame = new JFrame("Drawing 2D Shapes");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Bintang shapes2JPanel = new Bintang();
        frame.add(shapes2JPanel);
        frame.setBackground(Color.WHITE);
        frame.setSize(315, 330);
        frame.setVisible(true);
    }
}

