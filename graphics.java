package Modul6;

import javax.swing.*;
import java.awt.*;

public class graphics extends JPanel {

    public static void main(String[] a) {
        JFrame f = new JFrame();
        f.setTitle("Graphics");
        f.setSize(500, 700);
        f.add(new graphics());
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public void paint(Graphics g) {
        g.setFont(new Font("Arial",Font.PLAIN,18));

        g.setColor( new Color(73, 255, 222) );
        g.fillArc (25, 10, 120, 120, 30, 275);
        g.setColor(Color.GRAY);
        g.drawString( "Ini Adalah hasil fillArc(): ", 160, 80 );

        g.setColor( new Color(125, 200, 255) );
        g.fillOval(25, 150, 120, 120);
        g.setColor(Color.GRAY);
        g.drawString( "Ini Adalah hasil fillOval(): ", 160, 210 );

        int xpoints[] = {25, 145, 25, 145, 25};
        int ypoints[] = {310, 310, 430, 430, 310};
        int npoints = 5;
        g.setColor( new Color( 106, 121, 225) );
        g.fillPolygon(xpoints, ypoints, npoints);
        g.setColor(Color.GRAY);
        g.drawString( "Ini Adalah hasil fillPolygon(): ", 160, 370 );

        g.setColor( new Color(232, 255, 100) );
        g.fillRect(25,470,120,120);
        g.setColor(Color.GRAY);
        g.drawString( "Ini Adalah hasil fillRect(): ", 160, 530 );

    }
}
