package Modul6;

import javax.swing.*;
import java.awt.*;

public class Warna extends JPanel {

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.setBackground(Color.BLACK);
		
		g.setColor(new Color(255, 222, 185));
		g.fillRect(15, 25, 100, 20);
		g.drawString("Warna PeachPuff ", 130, 40);
		
		g.setColor(new Color(255,250,205));
		g.fillRect(15, 50, 100, 20);
		g.drawString("Warna Lemon Chiffon: ", 130, 65);
		
		g.setColor(new Color(220,220,220));
		g.fillRect(15, 75, 100, 20);
		g.drawString("Warna Gainsboro " , 130, 90);
		
		g.setColor(new Color(255,245,238));
		g.fillRect(15, 100, 100, 20);
		g.drawString("Warna Sea Shell ", 130, 115);
		
		g.setColor(new Color(230,230,250));
		g.fillRect(15, 125, 100, 20);
		g.drawString("Warna Lavender " , 130, 140);
		
		g.setColor(new Color(0,191,255));
		g.fillRect(15, 150, 100, 20);
		g.drawString("Warna Deep Sky Blue " , 130, 165);
		
		g.setColor(new Color(0,206,209));
		g.fillRect(15, 175, 100, 20);
		g.drawString("Warna Dark Torquoise ", 130, 190);
		
		g.setColor(new Color(143,188,143));
		g.fillRect(15, 200, 100, 20);
		g.drawString("Warna Dark Sea Green" , 130, 215);

		g.setColor(new Color(184,134,11));
		g.fillRect(15, 225, 100, 20);
		g.drawString("Warna Dark Goldenrod" , 130, 240);

		g.setColor(new Color(139,101,8));
		g.fillRect(15, 250, 100, 20);
		g.drawString("Warna Dark Goldenrod" , 130, 265);
		

		g.setColor(new Color(139,105,105));
		g.fillRect(15, 275, 100, 20);
		g.drawString("Warna Rosy Brown" , 130, 290);
		
		
				
	}

}
