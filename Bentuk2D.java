package Modul6;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

public class Bentuk2D extends JPanel {
    public static void main(String[] a) {
        JFrame f = new JFrame();
        Color color = new Color(125, 200, 255);
    	f.setBackground(color);
        f.setTitle("Macam - macam bentuk 2D");
        f.setSize(400, 500);
        f.add(new Bentuk2D());
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        double x = 10.5;
        double y = 10.5;
        int rectHeight = 100;
        int rectWidth  = 150;
        g2.draw( new Line2D.Double(x,  y + rectHeight - 1,  x + rectWidth , y));
        g2.drawString("Draw Line2D", 200, 80);
        
        double a = 10.5;
        double b = 130;
        int aheigth = 150;
        int awidth  = 70;
        Stroke stroke = new BasicStroke();
        g2.setStroke(stroke);
        g2.draw( new Rectangle2D.Double(a,  b, aheigth, awidth));
        g2.drawString("Draw Rectangle2D", 200, 170);
    
        float[] dash1 = { 2f, 0f, 2f };
        double c = 10.5;
        double d = 240;
        int cHeight = 70;
        int dWidth  = 150;
        Stroke dashed = new BasicStroke(1,BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_ROUND,1.0f,dash1,2f);
        g2.setStroke(dashed);
        g2.draw( new RoundRectangle2D.Double(c,  d, dWidth , cHeight, 20,20));
        g2.drawString("Draw RoundRectangle2D", 200, 280);
        
        Stroke wideStroke = new BasicStroke(12.0f);
        double e = 11;
        double f = 340;
        int eHeight = 100;
        int fWidth  = 200;
        int w = getSize().width;
        int h = getSize().height;
        g2.setStroke(wideStroke);
        g2.draw( new Arc2D.Double(e,  f, fWidth , eHeight,90,135,Arc2D.OPEN));
        g2.drawString("Draw Arc2D", 200, 380);
    }
}
