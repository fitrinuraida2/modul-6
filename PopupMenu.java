package Modul6;

import java.awt.BorderLayout;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.Color; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JColorChooser; 
import javax.swing.JPanel;

public class PopupMenu extends JFrame implements ActionListener {
	private Color color = Color.LIGHT_GRAY;
	private JPanel panel1, panel2;
	private JRadioButton blue, yellow, red;
	
	public PopupMenu() {
		blue = new JRadioButton("Blue");
		yellow = new JRadioButton("Yellow");
		red = new JRadioButton("Red");
		
		panel1 = new JPanel();
		panel2 = new JPanel();
		panel1.add(blue);
		panel1.add(yellow);
		panel1.add(red);
		getContentPane().add(panel2, BorderLayout.CENTER);
		panel2.setSize(200, 200);
		getContentPane().add(panel1, BorderLayout.PAGE_END);
		
		ButtonGroup group = new ButtonGroup();
		group.add(blue);
		group.add(yellow);
		group.add(red);
		
		blue.addActionListener(this);
		yellow.addActionListener(this);
		red.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == blue)
			panel2.setBackground(Color.BLUE);
		if(ae.getSource() == yellow)
			panel2.setBackground(Color.YELLOW);
		if(ae.getSource() == red)
			panel2.setBackground(Color.RED);
	}
	
	public static void main(String[] args) {
		PopupMenu frame = new PopupMenu();
		frame.setTitle("Using JPopupMenus");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600, 400);
		frame.setVisible(true);
	}
}
