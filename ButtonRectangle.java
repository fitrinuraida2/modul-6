package Modul6;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JColorChooser; 
import javax.swing.JPanel;

public class ButtonRectangle extends JFrame implements ActionListener {
	private Color color = Color.LIGHT_GRAY;
	private JPanel panel1, panel2;
	private JButton cyan, magenta, orange;
	
	public ButtonRectangle() {
		panel1 = new JPanel();
		panel1.setBackground(color);
		getContentPane().setBackground(Color.WHITE);
		
		cyan = new JButton("CYAN");
		magenta = new JButton("MAGENTA");
		orange = new JButton("ORANGE");
		panel1.add(cyan);
		panel1.add(magenta);
		panel1.add(orange);
		panel2 = new JPanel();
		getContentPane().add(panel2, BorderLayout.CENTER);
		panel2.setSize(200, 200);
		getContentPane().add(panel1, BorderLayout.PAGE_END);
		
		cyan.addActionListener(this);
		magenta.addActionListener(this);
		orange.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == cyan)
			panel2.setBackground(Color.CYAN);
		if(ae.getSource() == magenta)
			panel2.setBackground(Color.MAGENTA);
		if(ae.getSource() == orange)
			panel2.setBackground(Color.ORANGE);
	}
	
	public static void main(String[] args) {
		ButtonRectangle frame = new ButtonRectangle();
		frame.setTitle("TUGAS 6-2");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(320, 170);
		frame.setVisible(true);
	}
}

